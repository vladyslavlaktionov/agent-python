class FileConfig:
    files_path = 'files/'

    def get_file_path(self):
        return self.files_path

    def set_file_path(self, files_path):
        self.files_path = files_path
        return self.files_path
